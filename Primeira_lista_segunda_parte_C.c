 #include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[]) {

  int cont = 1;


  while (cont > 0) {
    printf("Escolha uma opção \n"
            "Opção 1 = Verificação de número par\n"
            "Opção 2 = Potência\n"
            "Opção 3 = Raiz\n"
            "Opção 4 = Verificação de ano bissexto\n"
            "Opção 5 = Média ponderada\n"
            "Opção 6 = Verificação de notas\n"
            "Opção 7 = Fatoração\n"
            "Opção 8 = Verificação de números par\n"
            "Opção 9 = Fibonacci\n"
            "Opção 10 = Matrícula\n");

    scanf("%d", &cont );
    if (cont == 0)
    {
        printf("O programa foi encerrado, até mais! ");
        cont = 0;
    }

    switch (cont)
    {
      case 1:{
          system("clear");
          printf("------------------Verificação de número par------------------\n \n");
          int num;

          printf("Digite um número para verificar se é par: " );
          scanf("%d", &num);
          if (num % 2 == 0 ){
                printf("O número é par \n\n");
          }
          else{
                printf("O número é impar \n\n");
          }

          break;

      }


      case 2:{
          system("clear");
          printf("------------------Potência------------------\n \n");
          int valorint;
          float valorfl;

          printf("Digite um numero quebrado: \n");
          scanf("%f", &valorfl);

          printf("Agora, digite um numero inteiro: \n");
          scanf("%d", &valorint);

          printf("A potenciacao dos numeros resulta em: %.2e\n\n", pow(valorfl, valorint));

          break;
        }

      case 3:{
          system("clear");
          printf("------------------Raiz------------------\n \n");
          int valorint;
          float valorfl;

          printf("Digite um número quebrado: ");
          scanf("%f", &valorfl);

          printf("Ok, agora digite um número inteiro: ");
          scanf("%d", &valorint);

          printf("A raiz quadrada do números resulta em : %.2e\n\n", pow(valorfl, 1.0/valorint));

          break;

        }
      case 4:{
          system("clear");
          printf("------------------Verificação de ano bissexto------------------\n \n");
          int ano;

          printf("Digite o ano: ");
          scanf("%d", &ano);
          if(ano % 4 == 0 ){
                printf("O ano é bissexto! \n \n");
          }

          else{
                printf("O ano não é bissexto! \n \n");
          }

          break;
        }

      case 5:{
          system("clear");
          printf("------------------Média ponderada------------------\n \n");
          float nota1, nota2, nota3, media;

          printf("Digite sua primeira nota: ");
          scanf("%f", &nota1);

          printf("Digite sua segunda nota: ");
          scanf("%f", &nota2);

          printf("Digite sua terceira nota: ");
          scanf("%f", &nota3);

          media = ((nota1*1)+(nota2*1)+(nota3*2))/4;

          if(media >= 60.0){
                printf("Sua média é: %.2f e você foi aprovado! \n\n", media);
          }

          else{
                printf("Sua média é: %.2f e você reprovou! \n\n", media);
          }

          break;
        }

      case 6:{
          system("clear");
          printf("------------------Verificação de notas------------------\n \n");
          float nota1, nota2, nota3, media;

          printf("Digite sua primeira nota: ");
          scanf("%f", &nota1);

          printf("Digite sua segunda nota: ");
          scanf("%f", &nota2);

          media = (nota1+nota2)/2;

          if(nota1 >=0.0 && nota1 <=10.0 && nota2 >=0.0 && nota2 <=10.0){
                printf("Sua média é: %.2f \n\n", media);
          }

          else{
                printf("Suas nota não são válidas! \n\n");
          }

          break;
      }

      case 7:{
          system("clear");
          printf("------------------Fatoração------------------\n \n");

          int fat, result;

          printf("Digite um número para o programa fatorar ele: ");
          scanf("%d", &result);

          for(fat = 1; result > 1; result = result - 1){
                fat = fat * result;
          }

          printf("O fatorial calculado foi: %d\n", fat);

       break;
      }


      case 8:{
          system("clear");
          printf("------------------Verificação de números par------------------\n \n");

          int i, num;
          int cont =0;

          printf("Digite um número para verificar se ele é primo ou não: ");
          scanf("%d", &num);

          for (i = 1; i <= num; i++){
                if (num % i == 0){
                    cont++;
                    break;
                    }
                }

          if (div == 2){
                printf("O número %d é um número primo! \n", num);
          }

          else{
                printf("O número %d não é primo! \n", num);
          }

          break;
      }
      case 9:{
        system("clear");
        printf("------------------Fibonacci------------------\n \n");

        int num, f1=0, f2=1, f3;

        printf("Digite um número: ");
        scanf("%d", &num);

        if(num < 0){
            printf("Número %d é inválido", num);
        }

        printf("A sequência fibonacci desse número é: ");
        printf("0 - 1");

        while (f2 <num){
            f3 = f2 + f1;
            printf(" - %d", f3);
            f1=f2;
            f2=f3;
        }
        printf("\n \n");

        break;
      }
      case 10:{
          system("clear");
          printf("------------------Matrícula------------------\n \n");
          int matricula;
          char nome[61];

          printf("Digite sua matrícula: ");
          scanf("%d", &matricula);

          printf("Digite seu nome: ");
          scanf("%s", nome);

          printf("\nSeu nome é: %s e sua matrícula é %x \n\n", nome, matricula);

          break;
      }
    }
  }
  return 0;
}
