#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char const *argv[]) {

  int cont = 1;


  while (cont > 0) {
    printf("Escolha uma opção \n"
            "Opção 1\n"
            "Opção 2\n"
            "Opção 3\n"
            "Opção 4\n"
            "Opção 5\n"
            "Opção 6\n"
            "Opção 7\n"
            "Opção 8\n"
            "Opção 9\n"
            "Opção 10\n");

    scanf("%d", &cont );
    if (cont == 0)
    {
        printf("O programa foi encerrado, até mais! ");
        cont = 0;
    }

    switch (cont)
    {
      case 1:{
        int num;

        printf("Digite um número para verificar se é par: " );
        scanf("%d", &num);

        if (num % 2 == 0 )
        {
          printf("O número é par \n\n");
        }

        else
        {
          printf("O número é impar \n\n");
        }
        }
        break;

      case 2:{
        int valorint;
        float valorfl;

        printf("Digite um numero quebrado: \n");
        scanf("%f", &valorfl);

        printf("Agora, digite um numero inteiro: \n");
        scanf("%d", &valorint);

        printf("A potenciacao dos numeros resulta em: %.2e\n\n", pow(valorfl, valorint));
        break;
        }

      case 3:{
        int valorint;
        float valorfl;

        printf("Digite um número quebrado: ");
        scanf("%f", &valorfl);

        printf("Ok, agora digite um número inteiro: ");
        scanf("%d", &valorint);

        printf("A raiz quadrada do números resulta em : %.2e\n\n", pow(valorfl, 1.0/valorint));
        break;

        }
      case 4:{
        int ano;

        printf("Digite o ano: ");
        scanf("%d", &ano);

        if(ano % 4 == 0 ){
            printf("O ano é bissexto! \n \n");
        }
        else {
            printf("O ano não é bissexto! \n \n");
        }
        break;
        }

      case 5:{
        float nota1, nota2, nota3, media;

        printf("Digite sua primeira nota: ");
        scanf("%f", &nota1);

        printf("Digite sua segunda nota: ");
        scanf("%f", &nota2);

        printf("Digite sua terceira nota: ");
        scanf("%f", &nota3);

        media = ((nota1*1)+(nota2*1)+(nota3*2))/4;

        if(media >= 60.0){
            printf("Sua média é: %.2f e você foi aprovado! \n\n", media);
        }

        else{
            printf("Sua média é: %.2f e você reprovou! \n\n", media);
        }

        break;
        }

      case 6:{
        float nota1, nota2, nota3, media;

        printf("Digite sua primeira nota: ");
        scanf("%f", &nota1);

        printf("Digite sua segunda nota: ");
        scanf("%f", &nota2);

        media = (nota1+nota2)/2;

        if(nota1 >=0.0 && nota1 <=10.0 && nota2 >=0.0 && nota2 <=10.0){
            printf("Sua média é: %.2f \n\n", media);
        }
        else{
            printf("Suas nota não são válidas! \n\n");
        }


      break;
      }

      case 7:

        printf("Come soon \n\n");
      break;

      case 8:

        printf("Come soon \n\n");
      break;

      case 9:

        printf("Come soon \n\n");
      break;


      case 10:{
        int matricula;

        printf("Digite sua matrícula: ");
        scanf("%d", &matricula);
        printf("Seu nome é: Paulo Cesar e sua matrícula é %x \n\n", matricula);


      break;
      }
    }
  }
  return 0;
}
